import QtQuick 2.0
import org.kde.kdecoration 1.0 as KDecoration
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras

Item {
    width: 1000
    height: 400
    KDecoration.Bridge {
        id: bridgeItem
        plugin: "org.kde.breeze"
    }

    KDecoration.Settings {
        id: settingsItem
        bridge: bridgeItem
    }

    PlasmaCore.DataSource {
        id: tasksSource
        dataEngine: "tasks"
        interval: 0
        onSourceAdded: {
            connectSource(source)
        }
        Component.onCompleted: {
            connectedSources = sources
        }
    }

    function performOperation(id, what) {
        var service = tasksSource.serviceForSource("tasks");
        var operation = service.operationDescription(what);
        operation["Id"] = id
        return service.startOperationCall(operation);
    }

    PlasmaExtras.ScrollArea {
        anchors.fill: parent
        ListView {
            id: tasksList

            orientation: ListView.Horizontal

            model: tasksSource.models["tasks"]

            delegate: Item {
                width: 400
                height: 400
                KDecoration.Decoration {
                    id: decoration
                    bridge: bridgeItem
                    settings: settingsItem
                    anchors.centerIn: parent
                    width:  thumbnail.paintedWidth + thumbnail.anchors.leftMargin + thumbnail.anchors.rightMargin
                    height: thumbnail.paintedHeight + thumbnail.anchors.topMargin + thumbnail.anchors.bottomMargin
                    Component.onCompleted: {
                        client.caption = Qt.binding(function() { return model["DisplayRole"]; });
                        client.icon = Qt.binding(function() { return model["DecorationRole"]; });
                        client.maximizedHorizontally = Qt.binding(function() { return model["Maximized"]; });
                        client.maximizedVertically = Qt.binding(function() { return model["Maximized"]; });
                        clientConnections.target = client;
                    }
                    Connections {
                        id: clientConnections
                        target: null
                        onCloseRequested: performOperation(model["Id"], "close")
                    }
                }

                PlasmaCore.WindowThumbnail {
                    id: thumbnail
                    anchors {
                        fill: parent
                        topMargin: decoration.decoration.borderTop + (decoration.decoration.shadow ? decoration.decoration.shadow.paddingTop : 0)
                        rightMargin: decoration.decoration.borderRight + (decoration.decoration.shadow ? decoration.decoration.shadow.paddingRight : 0)
                        bottomMargin: decoration.decoration.borderBottom + (decoration.decoration.shadow ? decoration.decoration.shadow.paddingBottom : 0)
                        leftMargin: decoration.decoration.borderLeft + (decoration.decoration.shadow ? decoration.decoration.shadow.paddingLeft : 0)
                    }
                    winId: model["WindowList"][0]
                }
            }
        }
    }
}
